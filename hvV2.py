# -*- coding: utf-8 -*-
"""
Created on Fri Jan 18 12:12:48 2019

@author: Alexander
"""

import pigpio
import time
scalefactor=1

def bitwiseleakage(byte1, byte2):
    return ((byte1<<8) | byte2)
    #ADCvalue/16bit*reference/amp/Rshunt need to substract null value

def bitwiseconv(byte1,byte2):
    Channel=(byte1>>4 & 7)+1  
    twelvebit=((byte1 & 15)<<8) | byte2
    return (Channel,round(twelvebit*scalefactor,4),twelvebit)


    
def calc_k(VHV):
    print(VHV*2.**10/1200.)
    return int(VHV*2.**10/1200.)
    
def calc_Uout(VHV):
    print(VHV/1200.*4.096)
    
class hvV2:

    def setHv(self,k):
        if k<= 1023 and k>=0 and isinstance(k, int):
            pi= pigpio.pi()
            dac=pi.i2c_open(1,0x10)
            try:
                pi.i2c_write_device(dac,(0b00110000,k>>2,(3&k)<<6))
            except pigpio.error as e: 
                pi.i2c_close(adc)
                pi.stop()
                return 0
            except:
                pi.i2c_close(adc)
                pi.stop()
                raise
            
            pi.i2c_close(dac)
            pi.stop()
            Uout=(k/2.**10)*4.096
            UHV=Uout/4.096*1200.
            print('set HV to {:.3f} V using {:.3f} V and {:0b}'.format(UHV,Uout,k))
        else:
            print('value error: k must be interger between 0 and 1023')
        return(None)
    
    def meaHvLeakRaw(self):
        pi=pigpio.pi()
        adc=pi.i2c_open(1,0x14)
        try:
            pi.i2c_read_device(adc,2)
            time.sleep(0.01)
            count,data=pi.i2c_read_device(adc,2) #read shunt voltage
        except pigpio.error as e: 
            pi.i2c_close(adc)
            pi.stop()
            return 0
        except:
            pi.i2c_close(adc)
            pi.stop()
            raise
     
        pi.i2c_close(adc)
        pi.stop()
        #print('{:016b}, {:d}'.format(bitwiseleakage(data[0],data[1]),bitwiseleakage(data[0],data[1])))
        #print('measured HV leakage current, it is {:f} nA'.format(bitwiseleakage(data[0],data[1])/(2.**16-1.)*5./201.4/10.**5*10.**9))
        #print('measured HV leakage current, it is {:f} nA'.format(bitwiseleakage(data[0],data[1])/(2.**16-1.)*5./201.4/10.**5*10.**9-0.5*5./201.4/10.**5*10.**9))
        
        return(bitwiseleakage(data[0],data[1]))
    
    def meaHvLeak(self):
        pi=pigpio.pi()
        adc=pi.i2c_open(1,0x14)
        try:
            pi.i2c_read_device(adc,2)
            time.sleep(0.1)
            count,data=pi.i2c_read_device(adc,2) #read shunt voltage
        except pigpio.error as e: 
            pi.i2c_close(adc)
            pi.stop()
            return 0
        except:
            pi.i2c_close(adc)
            pi.stop()
            raise
        pi.i2c_close(adc)
        pi.stop()
        print('{:016b}, {:d}'.format(bitwiseleakage(data[0],data[1]),bitwiseleakage(data[0],data[1])))
        #print('measured HV leakage current, it is {:f} nA'.format(bitwiseleakage(data[0],data[1])/(2.**16-1.)*5./201.4/10.**5*10.**9))
        #print('measured HV leakage current, it is {:f} nA'.format(bitwiseleakage(data[0],data[1])/(2.**16-1.)*5./201.4/10.**5*10.**9-0.5*5./201.4/10.**5*10.**9))
        print('measured HV leakage current, it is {:f} nA'.format(bitwiseleakage(data[0],data[1])/(2.**16-1.)*5./201.4/4.99/10.**4*10.**9))
        print('measured HV leakage current, it is {:f} nA'.format(bitwiseleakage(data[0],data[1])/(2.**16-1.)*5./201.4/4.99/10.**4*10.**9-0.5*5./201.4/4.99/10.**4*10.**9))
        
        return(bitwiseleakage(data[0],data[1]))
    #ADCvalue/16bit*reference/amp/Rshunt need to substract null value
