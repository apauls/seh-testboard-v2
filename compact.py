# -*- coding: utf-8 -*-
"""
Created on Tue Feb  5 11:46:46 2019

@author: Alexander
"""

from adc import adc
import temp
import opcodes 
import gpio
import hvV2
from ina import ina 
import numpy as np

tbadc = adc()
tbina = ina()

def LV_Values():
    Mon=tbadc.meaMon()
    P2V5_M=tbadc.meaP2V5_M()
    P1V5=tbadc.meaP1V5()
    P1V2=tbadc.meaP1V2()
    Rssi_l=tbadc.meaRssi_l()
    Vhvj7=tbadc.meaVhvj7()
    Vhvj8=tbadc.meaVhvj8()
    Rssi_l=tbadc.meaRssi_l()
    
    VBusIn=tbina.meaVBusIn()
    VShuntIn=tbina.meaVShuntIn()
    CurrentIn=tbina.meaCurrentIn()
    PowerIn=tbina.meaPowerIn()
    
    VBusOutR=tbina.meaVBusOutR()
    VShuntOutR=tbina.meaVShuntOutR()
    CurrentOutR=tbina.meaCurrentOutR()
    PowerOutR=tbina.meaPowerOutR()
    
    VBusOutL=tbina.meaVBusOutL()
    VShuntOutL=tbina.meaVShuntOutL()
    CurrentOutL=tbina.meaCurrentOutL()
    PowerOutL=tbina.meaPowerOutL()

    print('{:<30}{:>012b}{:>20}'.format('ADC channel 1 (Mon): ',Mon, str(np.round(Mon*2.048/4095,4))+' V (1/1000 HV)'))
    print('{:<30}{:>012b}{:>20}'.format('ADC channel 2 (P2V5_M): ',P2V5_M, str(np.round(P2V5_M*10*2.048/4095,4))+' V'))
    print('{:<30}{:>012b}{:>20}'.format('ADC channel 3 (P1V5): ',P1V5, str(np.round(P1V5*2.048/4095,4))+' V'))
    print('{:<30}{:>012b}{:>20}'.format('ADC channel 4 (P1V2): ',P1V2, str(np.round(P1V2*2.048/4095,4))+' V'))
    print('{:<30}{:>012b}{:>20}'.format('ADC channel 5 (Rssi_l): ',Rssi_l, str(np.round(Rssi_l*2.048/4095,4))+' V'))
    print('{:<30}{:>012b}{:>20}'.format('ADC channel 6 (Vhvj7): ',Vhvj7, str(np.round(Vhvj7*2.048/4095*1001,4))+' V'))
    print('{:<30}{:>012b}{:>20}'.format('ADC channel 7 (Vhvj8): ',Vhvj8, str(np.round(Vhvj8*2.048/4095*1001,4))+' V'))
    print('{:<30}{:>012b}{:>20}'.format('ADC channel 8 (Rssi_l): ',Rssi_l, str(np.round(Rssi_l*2.048/4095,4))+' V'))
    print('')
    print('{:<17}{:>015b}{:>15.3f} V'.format('VBusIn: ',VBusIn,VBusIn*1.25e-3))
    print('{:<17}{:>015b}{:>15.3f} muV'.format('VShuntIn: ',VShuntIn,VShuntIn*2.5e-6))
    print('{:<17}{:>015b}{:>15.3f} A'.format('CurrentIn: ',CurrentIn,CurrentIn*200e-6)) #new lsb 200muA/bit
    print('{:<17}{:>015b}{:>15.3f} W'.format('PowerIn: ',PowerIn,PowerIn*5e-3)) #new lsb: 200muW/bit*25
    print('')
    print('{:<17}{:>015b}{:>15.3f} V'.format('VBusOutR: ',VBusOutR,VBusOutR*1.25e-3))
    print('{:<17}{:>015b}{:>15.3f} muV'.format('VShuntOutR: ',VShuntOutR,VShuntOutR*2.5e-6))
    print('{:<17}{:>015b}{:>15.3f} A'.format('CurrentOutR: ',CurrentOutR,CurrentOutR*200e-6)) #new lsb 200muA/bit
    print('{:<17}{:>015b}{:>15.3f} W'.format('PowerOutR: ',PowerOutR,PowerOutR*5e-3)) #new lsb: 200muW/bit*25
    print('')

    print('{:<17}{:>015b}{:>15.3f} V'.format('VBusOutL: ',VBusOutL,VBusOutL*1.25e-3))
    print('{:<17}{:>015b}{:>15.3f} muV'.format('VShuntOutL: ',VShuntOutL,VShuntOutL*2.5e-6))
    print('{:<17}{:>015b}{:>15.3f} A'.format('CurrentOutL: ',CurrentOutR,CurrentOutL*200e-6)) #new lsb 200muA/bit
    print('{:<17}{:>015b}{:>15.3f} W'.format('PowerOutL: ',PowerOutR,PowerOutL*5e-3)) #new lsb: 200muW/bit*25


"""
class section needs rework
"""
class LVReader:
    tbadc = adc()
    tbina = ina()

    def LV_Values1(self):
        Mon = tbadc.mea_Mon()
        P1V25_L = tbadc.mea_P1V25_L()
        VP2V5 = tbadc.mea_VP2V5()
        P1V25_R = tbadc.mea_P1V25_R()
        RSSI_L = tbadc.mea_RSSI_L()
        VHVJ7 = tbadc.mea_VHVJ7()
        VHVJ8 = tbadc.mea_VHVJ8()
        RSSI_R = tbadc.mea_RSSI_R()
        VBUS = tbina.mea_VBUS()
        VSHUNT = tbina.mea_VSHUNT()
        CURRENT = tbina.mea_CURRENT()
        POWER = tbina.mea_POWER()

        return_tuple = (round(Mon*2.048/4095,4)*1000., # ADC_chan1 #np.round(Mon*2.048/4095,4).item() # .item() konvertiert von numpy float in python float #python round mach symmetrisches Runden
                        round(P1V25_L*2.048/4095,4), # ADC_chan2
                        round(VP2V5*2.048/4095*2,4), #ADC_chan3
                        round(P1V25_R*2.048/4095,4), #ADC_chan4
                        round(RSSI_L*2.048/4095,4), #ADC_chan5
                        round(VHVJ7*2.048/4095*1001,4), #ADC_chan6
                        round(VHVJ8*2.048/4095*1001,4), #ADC_chan7
                        round(RSSI_R*2.048/4095,4), #ADC_chan8
                        VBUS*1.25e-3,
                        VSHUNT*2.5e-6,
                        CURRENT*50e-6,
                        POWER*1.25e-3)

        return return_tuple