# -*- coding: utf-8 -*-
"""
Created on Thu Feb  7 13:24:24 2019

@author: Alexander
"""

import adc
import temp
import opcodes 
import gpio
import hv_v2
import ina 

import numpy as np
import ConfigParser 
import time 

class OwnConfigParser(ConfigParser.SafeConfigParser):
    #def __init__(self):

    def Add(self,zwei):
        return(ini_add(self,zwei))
    def Add_list(self,section,liste):
        for i in liste:
            self.set(section,i[0],i[1])
        return self

def ini_add(ini1, ini2):
    sec=ini2.sections()
    for i in sec:
        ini1.add_section(i)
        key=ini2.items(i)
        for j in key:
            ini1.set(i,j[0],j[1])
    return ini1

def ADC_block():
    data=[]
    data.append(('datatype','decimal adc count'))
    data.append(('timestamp',time.time()))
    data.append(('Mon','{:d}'.format(adc.mea_Mon())))
    data.append(('P1V25_L','{:d}'.format(adc.mea_P1V25_L())))
    data.append(('VP2V5','{:d}'.format(adc.mea_VP2V5())))
    data.append(('P1V25_R','{:d}'.format(adc.mea_P1V25_R())))
    data.append(('RSSI_L','{:d}'.format(adc.mea_RSSI_L())))
    data.append(('VHVJ7','{:d}'.format(adc.mea_VHVJ7())))
    data.append(('VHVJ8','{:d}'.format(adc.mea_VHVJ8())))
    data.append(('RSSI_R','{:d}'.format(adc.mea_RSSI_R())))
    print data
    return(data)

adc_count=1
config=OwnConfigParser()
for i in range(20):
    section='ADC {:d}'.format(i)
    config.add_section(section)
    config.Add_list(section,ADC_block())

with open('example.cfg', 'w') as configfile:
    config.write(configfile)