# -*- coding: utf-8 -*-
"""
Created on Fri Mar 16 13:49:52 2018

@author: Alexander

all measurements follow the same scheme

def mea_VHVRet():    #name of desired voltage
    second_scale_factor=1  #depends on voltage devider, currently unused
    pi= pigpio.pi() #connect to local pi
    adc=pi.i2c_open(1,0x21) #setup adc address
    pi.i2c_write_device(adc,(0b00000010,0b00000000,0b00011000)) # write the desired channel to the configuration resgister; hier Channel 1
    data=pi.i2c_zip(adc,(0x07,0x01,0x70,0x06,0x02,0x00)) #enable Mode 2 for previous selceted channels and select the conversion result register
    # and read 2 byte data for each adc channel
    pi.i2c_close(adc) #disconnect from adc
    pi.stop() #disconnect from pi
    print('measured channel 1, U_VHVRet is {:.3f} V'.format((((data[1][0] & 15)<<8) | data[1][1])*2.048/4095))
    # perform the conversion to voltage
    return((((data[1][0] & 15)<<8) | data[1][1])*2.048/4095*second_scale_factor)


"""

import pigpio
import time
class adc:
    
    def meaMon(self):
        #second_scale_factor=1
        pi= pigpio.pi()
        adc=pi.i2c_open(1,0x21)
        try:
            pi.i2c_write_device(adc,(0b00000010,0b00000000,0b00011000))
            data=pi.i2c_zip(adc,(0x07,0x01,0x70,0x06,0x02,0x00)) 
        except pigpio.error as e: 
            pi.i2c_close(adc)
            pi.stop()
            return 0
        except:
            pi.i2c_close(adc)
            pi.stop()
            raise
        pi.i2c_close(adc)
        pi.stop()
        #print('measured channel 1, Mon is {:.3f} V'.format((((data[1][0] & 15)<<8) | data[1][1])*2.048/4095))
        return((((data[1][0] & 15)<<8) | data[1][1]))
        
    def meaP2V5_M(self):
        second_scale_factor=10
        pi= pigpio.pi()
        adc=pi.i2c_open(1,0x21)
        try:
             pi.i2c_write_device(adc,(0b00000010,0b00000000,0b00101000)) # macht jedes mal einen neuen Thread auf der nciht endet, das muss man noch untersuchen!
             data=pi.i2c_zip(adc,(0x07,0x01,0x70,0x06,0x02,0x00))
        except pigpio.error as e: 
            pi.i2c_close(adc)
            pi.stop()
            return 0
        except:
            pi.i2c_close(adc)
            pi.stop()
            raise
        #time.sleep(0.2) # ohne sleep können zu schnell hintereinander Befehle gesendet werden
        pi.i2c_close(adc)
        pi.stop()
    
        
        #print('measured channel 2, P2V5_M is {:.3f} V'.format((((data[1][0] & 15)<<8) | data[1][1])*2.048/4095*second_scale_factor))
        return((((data[1][0] & 15)<<8) | data[1][1]))

    def meaP1V5(self):
        pi= pigpio.pi()
        adc=pi.i2c_open(1,0x21)
        try:
            pi.i2c_write_device(adc,(0b00000010,0b00000000,0b01001000))
            data=pi.i2c_zip(adc,(0x07,0x01,0x70,0x06,0x02,0x00)) 
        except pigpio.error as e: 
            pi.i2c_close(adc)
            pi.stop()
            return 0
        except:
            pi.i2c_close(adc)
            pi.stop()
            raise
        pi.i2c_close(adc)
        pi.stop()
        #print('measured channel 3, P1V5 is {:.3f} V'.format((((data[1][0] & 15)<<8) | data[1][1])*2.048/4095))
        return((((data[1][0] & 15)<<8) | data[1][1]))
    

    def meaP1V2(self):
        pi= pigpio.pi()
        adc=pi.i2c_open(1,0x21)
        try:
            pi.i2c_write_device(adc,(0b00000010,0b00000000,0b10001000))
            data=pi.i2c_zip(adc,(0x07,0x01,0x70,0x06,0x02,0x00)) 
        except pigpio.error as e: 
            pi.i2c_close(adc)
            pi.stop()
            return 0
        except:
            pi.i2c_close(adc)
            pi.stop()
            raise
        pi.i2c_close(adc)
        pi.stop()
        #print('measured channel 4, P1V2 is {:.3f} V'.format((((data[1][0] & 15)<<8) | data[1][1])*2.048/4095))
        return((((data[1][0] & 15)<<8) | data[1][1]))
    
    def meaRssi_l(self):
        pi= pigpio.pi()
        adc=pi.i2c_open(1,0x21)
        try:
            pi.i2c_write_device(adc,(0b00000010,0b00000001,0b00001000))
            data=pi.i2c_zip(adc,(0x07,0x01,0x70,0x06,0x02,0x00)) 
        except pigpio.error as e: 
            pi.i2c_close(adc)
            pi.stop()
            return 0
        except:
            pi.i2c_close(adc)
            pi.stop()
            raise
        pi.i2c_close(adc)
        pi.stop()
        #print('measured channel 5, U_RSSI_L is {:.3f} V'.format((((data[1][0] & 15)<<8) | data[1][1])*2.048/4095))
        return((((data[1][0] & 15)<<8) | data[1][1]))

    def meaVhvj7(self):
        pi= pigpio.pi()
        adc=pi.i2c_open(1,0x21)
        try:
            pi.i2c_write_device(adc,(0b00000010,0b00000010,0b00001000))
            data=pi.i2c_zip(adc,(0x07,0x01,0x70,0x06,0x02,0x00)) 
        except pigpio.error as e: 
            pi.i2c_close(adc)
            pi.stop()
            return 0
        except:
            pi.i2c_close(adc)
            pi.stop()
            raise
        pi.i2c_close(adc)
        pi.stop()
        #print('measured channel 6, U_VHVJ7 is {:.3f} V'.format((((data[1][0] & 15)<<8) | data[1][1])*2.048/4095))
        return((((data[1][0] & 15)<<8) | data[1][1]))

    def meaVhvj8(self):
        pi= pigpio.pi()
        adc=pi.i2c_open(1,0x21)
        try:
            pi.i2c_write_device(adc,(0b00000010,0b00000100,0b00001000))
            data=pi.i2c_zip(adc,(0x07,0x01,0x70,0x06,0x02,0x00)) 
        except pigpio.error as e: 
            pi.i2c_close(adc)
            pi.stop()
            return 0
        except:
            pi.i2c_close(adc)
            pi.stop()
            raise
        pi.i2c_close(adc)
        pi.stop()
        #print('measured channel 7, U_VHVJ8 is {:.3f} V'.format((((data[1][0] & 15)<<8) | data[1][1])*2.048/4095))
        return((((data[1][0] & 15)<<8) | data[1][1]))
    
    def meaRssi_r(self):
        pi= pigpio.pi()
        adc=pi.i2c_open(1,0x21)
        try:
            pi.i2c_write_device(adc,(0b00000010,0b00001000,0b00001000))
            data=pi.i2c_zip(adc,(0x07,0x01,0x70,0x06,0x02,0x00)) 
        except pigpio.error as e: 
            pi.i2c_close(adc)
            pi.stop()
            return 0
        except:
            pi.i2c_close(adc)
            pi.stop()
            raise
        pi.i2c_close(adc)
        pi.stop()
        #print('measured channel 8, U_RSSI_R is {:.3f} V'.format((((data[1][0] & 15)<<8) | data[1][1])*2.048/4095))
        return((((data[1][0] & 15)<<8) | data[1][1]))

    def channel(self, n):
        channels=['Mon','P2V5_M','P1V5','P1V2','Rssi_l','Vhvj7','Vhvj8','Rssi_r']
        if n<9 and n>0:
            return(eval('mea'+channels[n-1]+'()'))
        else:
            print('Error: channel out of bounds')
            return(None)

