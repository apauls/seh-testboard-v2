# -*- coding: utf-8 -*-
"""
Created on Fri Mar 16 11:14:38 2018

@author: Alexander

execute opcode(n) in command shell to execute opcode_n from the file opcodes.py

"""
import pigpio
import opcodes
class fpga:
    def opcode(self,n):
        try:
            return(getattr(opcodes,'opcode_'+"{:02d}".format(n))())
        except AttributeError:
            print('Error: opcode not found')
            return(None)









