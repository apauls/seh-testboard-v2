# -*- coding: utf-8 -*-
"""
Created on Fri Mar 16 14:23:36 2018

@author: Alexander
"""

import RPi.GPIO as GPIO
class gpio:
    def lvonOn(self):
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(40,GPIO.OUT)
        GPIO.output(40,GPIO.HIGH)
        #print('LVON is on')
        return(None)
    
    def lvonStatus(self):
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(40,GPIO.OUT)
        return(GPIO.input(40))
        
    def hvonOn(self):
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(38,GPIO.OUT)
        GPIO.output(38,GPIO.HIGH)
        #print('HVON is on')
        return(None)
    
    def hvonStatus(self):
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(38,GPIO.OUT)
        return(GPIO.input(38))

    def hvoffOn(self):
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(37,GPIO.OUT)
        GPIO.output(37,GPIO.HIGH)
        #print('HVOFF is on')
        return(None)
        
    def hvoffStatus(self):
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(37,GPIO.OUT)
        return(GPIO.input(37))
    
    def hvmonx8On(self):
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(36,GPIO.OUT)
        GPIO.output(36,GPIO.HIGH)
        #print('HVMONX8 is on')
        return(None)
    
    def hvmonx8Status(self):
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(36,GPIO.OUT)
        return(GPIO.input(36))
    
    def hvmonx7On(self):
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(35,GPIO.OUT)
        GPIO.output(35,GPIO.HIGH)
        #print('HVMONX7 is on')
        return(None)
        
    def hvmonx7Status(self):
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(35,GPIO.OUT)
        return(GPIO.input(35))
            
    def lvonOff(self):
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(40,GPIO.OUT)
        GPIO.output(40,GPIO.LOW)
        #print('LVON is off')
        return(None)
        
    def hvonOff(self):
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(38,GPIO.OUT)
        GPIO.output(38,GPIO.LOW)
        #print('HVON is off')
        return(None)
        
    def hvoffOff(self):
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(37,GPIO.OUT)
        GPIO.output(37,GPIO.LOW)
        #print('HVOFF is off')
        return(None)
    
    def hvmonx8Off(self):
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(36,GPIO.OUT)
        GPIO.output(36,GPIO.LOW)
        #print('HVMONX8 is off')
        return(None)

    def hvmonx7Off(self):
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(35,GPIO.OUT)
        GPIO.output(35,GPIO.LOW)
        #print('HVMONX7 is off')
        return(None)
                
    def runOff(self):
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(32,GPIO.OUT)
        GPIO.output(32,GPIO.LOW)
        #print('Run is off')
        return(None)
        
    def runOn(self):
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(32,GPIO.OUT)
        GPIO.output(32,GPIO.HIGH)
        #print('Run is on')
        return(None)
        
    def runStatus(self):
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(32,GPIO.OUT)
        return(GPIO.input(32))