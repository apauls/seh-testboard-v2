# -*- coding: utf-8 -*-
"""
Created on Fri Mar 16 13:49:52 2018

@author: Alexander


"""

import pigpio
import time
class ina:
    
    def _bitwiseshunt(self, list): #these functions are private: "_xxx"
        if list[0]<128:
            return((((list[0] & 127)<<8) | list[1]))
        else:
            return(((-1<<16) | (list[0]<<8) | (list[1])))

    def _bitwisecurrent(self, list):
        if list[0]<128:
            return((((list[0] & 127)<<8) | list[1]))
        else:
            return(((-1<<16) | (list[0]<<8) | (list[1])))

    def _bitwisepower(self, list):
        if list[0]<128:
            return((((list[0] & 127)<<8) | list[1]))
        else:
            return(((-1<<16) | (list[0]<<8) | (list[1])))

    def meaVBusIn(self):
        pi=pigpio.pi()
        ina=pi.i2c_open(1,0x40)
        try:
            pi.i2c_write_device(ina,(0b00000000,0b01000001,0b00100111)) #write configuration register, 
        #1 average, 
        #1.1 ms bus voltage conversion time, 
        #1.1 ms shunt voltage conversion time,
        #shunt and bus continuous mode
            pi.i2c_write_device(ina,(0b00000101,0b00000101,0b00000000)) #write calibration register
        #200 muA/bit = 1280 with 0.02 Ohms
            time.sleep(0.01)
            pi.i2c_write_byte(ina,0b0010) #set address register to bus voltage register
            count,data=pi.i2c_read_device(ina,2) #read bus voltage
        except pigpio.error as e: 
            pi.i2c_close(ina)
            pi.stop()
            return 0
        except:
            pi.i2c_close(ina)
            pi.stop()
            raise
        pi.i2c_close(ina)
        pi.stop()
        #print('measured bus voltage, it is {:.3f} V'.format((((data[0] & 127)<<8) | data[1])))
        return((((data[0] & 127)<<8) | data[1]))

    def meaVShuntIn(self):
        pi=pigpio.pi()
        ina=pi.i2c_open(1,0x40)
        try:
            pi.i2c_write_device(ina,(0b00000000,0b01000001,0b00100111)) #write configuration register, 
        #1 average, 
        #1.1 ms bus voltage conversion time, 
        #1.1 ms shunt voltage conversion time,
        #shunt and bus continuous mode
            pi.i2c_write_device(ina,(0b00000101,0b00000101,0b00000000)) #write calibration register
        #200 muA/bit = 1280 with 0.02 Ohms
            time.sleep(0.01)
            pi.i2c_write_byte(ina,0b0001) #set address register to shunt voltage register
            count,data=pi.i2c_read_device(ina,2) #read shunt voltage
        except pigpio.error as e: 
            pi.i2c_close(ina)
            pi.stop()
            return 0
        except:
            pi.i2c_close(ina)
            pi.stop()
            raise
        pi.i2c_close(ina)
        pi.stop()
        #print('measured shunt voltage, it is {:.3f} V'.format(bitwiseshunt(data)))
        return(self._bitwiseshunt(data))
        
    def meaCurrentIn(self):
        pi=pigpio.pi()
        ina=pi.i2c_open(1,0x40)
        try:
            pi.i2c_write_device(ina,(0b00000000,0b01000001,0b00100111)) #write configuration register, 
        #1 average, 
        #1.1 ms bus voltage conversion time, 
        #1.1 ms shunt voltage conversion time,
        #shunt and bus continuous mode
            pi.i2c_write_device(ina,(0b00000101,0b00000101,0b00000000)) #write calibration register
        #200 muA/bit = 1280 with 0.02 Ohms
            time.sleep(0.01)
            pi.i2c_write_byte(ina,0b0100) #set address register to current register
            count,data=pi.i2c_read_device(ina,2) #read current 
        except pigpio.error as e: 
            pi.i2c_close(ina)
            pi.stop()
            return 0
        except:
            pi.i2c_close(ina)
            pi.stop()
            raise
        pi.i2c_close(ina)
        pi.stop()
        #print('measured current, it is {:.3f} A'.format(bitwisecurrent(data)))
        return(self._bitwisecurrent(data))

    def meaPowerIn(self):
        pi=pigpio.pi()
        ina=pi.i2c_open(1,0x40)
        try:
            pi.i2c_write_device(ina,(0b00000000,0b01000001,0b00100111)) #write configuration register, 
        #1 average, 
        #1.1 ms bus voltage conversion time, 
        #1.1 ms shunt voltage conversion time,
        #shunt and bus continuous mode
            pi.i2c_write_device(ina,(0b00000101,0b00000101,0b00000000)) #write calibration register
        #200 muA/bit = 1280 with 0.02 Ohms
            time.sleep(0.01)
            pi.i2c_write_byte(ina,0b0011) #set address register to power register
            count,data=pi.i2c_read_device(ina,2) #read power
        except pigpio.error as e: 
            pi.i2c_close(ina)
            pi.stop()
            return 0
        except:
            pi.i2c_close(ina)
            pi.stop()
            raise
        pi.i2c_close(ina)
        pi.stop()
        #print('measured power, it is {:.3f} W'.format(bitwisepower(data)))
        return(self._bitwisepower(data))
        
    def meaVBusOutR(self):
        pi=pigpio.pi()
        ina=pi.i2c_open(1,0x44)
        try:
            pi.i2c_write_device(ina,(0b00000000,0b01000001,0b00100111)) #write configuration register, 
        #1 average, 
        #1.1 ms bus voltage conversion time, 
        #1.1 ms shunt voltage conversion time,
        #shunt and bus continuous mode
            pi.i2c_write_device(ina,(0b00000101,0b00000101,0b00000000)) #write calibration register
        #200 muA/bit = 1280 with 0.02 Ohms
            time.sleep(0.01)
            pi.i2c_write_byte(ina,0b0010) #set address register to bus voltage register
            count,data=pi.i2c_read_device(ina,2) #read bus voltage
        except pigpio.error as e: 
            pi.i2c_close(ina)
            pi.stop()
            return 0
        except:
            pi.i2c_close(ina)
            pi.stop()
            raise
        pi.i2c_close(ina)
        pi.stop()
        #print('measured bus voltage, it is {:.3f} V'.format((((data[0] & 127)<<8) | data[1])))
        return((((data[0] & 127)<<8) | data[1]))


    def meaVShuntOutR(self):
        pi=pigpio.pi()
        ina=pi.i2c_open(1,0x44)
        try:
            pi.i2c_write_device(ina,(0b00000000,0b01000001,0b00100111)) #write configuration register, 
        #1 average, 
        #1.1 ms bus voltage conversion time, 
        #1.1 ms shunt voltage conversion time,
        #shunt and bus continuous mode
            pi.i2c_write_device(ina,(0b00000101,0b00000101,0b00000000)) #write calibration register
        #200 muA/bit = 1280 with 0.02 Ohms
            time.sleep(0.01)
            pi.i2c_write_byte(ina,0b0001) #set address register to shunt voltage register
            count,data=pi.i2c_read_device(ina,2) #read shunt voltage
        except pigpio.error as e: 
            pi.i2c_close(ina)
            pi.stop()
            return 0
        except:
            pi.i2c_close(ina)
            pi.stop()
            raise
        pi.i2c_close(ina)
        pi.stop()
        #print('measured shunt voltage, it is {:.3f} V'.format(bitwiseshunt(data)))
        return(self._bitwiseshunt(data))
        
    def meaCurrentOutR(self):
        pi=pigpio.pi()
        ina=pi.i2c_open(1,0x44)
        try:
            pi.i2c_write_device(ina,(0b00000000,0b01000001,0b00100111)) #write configuration register, 
        #1 average, 
        #1.1 ms bus voltage conversion time, 
        #1.1 ms shunt voltage conversion time,
        #shunt and bus continuous mode
            pi.i2c_write_device(ina,(0b00000101,0b00000101,0b00000000)) #write calibration register
        #200 muA/bit = 1280 with 0.02 Ohms
            time.sleep(0.01)
            pi.i2c_write_byte(ina,0b0100) #set address register to current register
            count,data=pi.i2c_read_device(ina,2) #read current
        except pigpio.error as e: 
            pi.i2c_close(ina)
            pi.stop()
            return 0
        except:
            pi.i2c_close(ina)
            pi.stop()
            raise
        pi.i2c_close(ina)
        pi.stop()
        #print('measured current, it is {:.3f} A'.format(bitwisecurrent(data)))
        return(self._bitwisecurrent(data))

    def meaPowerOutR(self):
        pi=pigpio.pi()
        ina=pi.i2c_open(1,0x44)
        try:
            pi.i2c_write_device(ina,(0b00000000,0b01000001,0b00100111)) #write configuration register, 
        #1 average, 
        #1.1 ms bus voltage conversion time, 
        #1.1 ms shunt voltage conversion time,
        #shunt and bus continuous mode
            pi.i2c_write_device(ina,(0b00000101,0b00000101,0b00000000)) #write calibration register
        #200 muA/bit = 1280 with 0.02 Ohms
            time.sleep(0.01)
            pi.i2c_write_byte(ina,0b0011) #set address register to power register
            count,data=pi.i2c_read_device(ina,2) #read power
        except pigpio.error as e: 
            pi.i2c_close(ina)
            pi.stop()
            return 0
        except:
            pi.i2c_close(ina)
            pi.stop()
            raise
        pi.i2c_close(ina)
        pi.stop()
        #print('measured power, it is {:.3f} W'.format(bitwisepower(data)))
        return(self._bitwisepower(data))

        
    def meaVBusOutL(self):
        pi=pigpio.pi()
        ina=pi.i2c_open(1,0x45)
        try:
            pi.i2c_write_device(ina,(0b00000000,0b01000001,0b00100111)) #write configuration register, 
        #1 average, 
        #1.1 ms bus voltage conversion time, 
        #1.1 ms shunt voltage conversion time,
        #shunt and bus continuous mode
            pi.i2c_write_device(ina,(0b00000101,0b00000101,0b00000000)) #write calibration register
        #200 muA/bit = 1280 with 0.02 Ohms
            time.sleep(0.01)
            pi.i2c_write_byte(ina,0b0010) #set address register to bus voltage register
            count,data=pi.i2c_read_device(ina,2) #read bus voltage
        except pigpio.error as e: 
            pi.i2c_close(ina)
            pi.stop()
            return 0
        except:
            pi.i2c_close(ina)
            pi.stop()
            raise
        pi.i2c_close(ina)
        pi.stop()
        #print('measured bus voltage, it is {:.3f} V'.format((((data[0] & 127)<<8) | data[1])))
        return((((data[0] & 127)<<8) | data[1]))  



    def meaVShuntOutL(self):
        pi=pigpio.pi()
        ina=pi.i2c_open(1,0x45)
        try:
            pi.i2c_write_device(ina,(0b00000000,0b01000001,0b00100111)) #write configuration register, 
        #1 average, 
        #1.1 ms bus voltage conversion time, 
        #1.1 ms shunt voltage conversion time,
        #shunt and bus continuous mode
            pi.i2c_write_device(ina,(0b00000101,0b00000101,0b00000000)) #write calibration register
        #200 muA/bit = 1280 with 0.02 Ohms
            time.sleep(0.01)
            pi.i2c_write_byte(ina,0b0001) #set address register to shunt voltage register
            count,data=pi.i2c_read_device(ina,2) #read shunt voltage
        except pigpio.error as e: 
            pi.i2c_close(ina)
            pi.stop()
            return 0
        except:
            pi.i2c_close(ina)
            pi.stop()
            raise
        pi.i2c_close(ina)
        pi.stop()
        #print('measured shunt voltage, it is {:.3f} V'.format(bitwiseshunt(data)))
        return(self._bitwiseshunt(data))
        
    def meaCurrentOutL(self):
        pi=pigpio.pi()
        ina=pi.i2c_open(1,0x45)
        try:
            pi.i2c_write_device(ina,(0b00000000,0b01000001,0b00100111)) #write configuration register, 
        #1 average, 
        #1.1 ms bus voltage conversion time, 
        #1.1 ms shunt voltage conversion time,
        #shunt and bus continuous mode
            pi.i2c_write_device(ina,(0b00000101,0b00000101,0b00000000)) #write calibration register
        #200 muA/bit = 1280 with 0.02 Ohms
            time.sleep(0.01)
            pi.i2c_write_byte(ina,0b0100) #set address register to current register
            count,data=pi.i2c_read_device(ina,2) #read current 
        except pigpio.error as e: 
            pi.i2c_close(ina)
            pi.stop()
            return 0
        except:
            pi.i2c_close(ina)
            pi.stop()
            raise
        pi.i2c_close(ina)
        pi.stop()
        #print('measured current, it is {:.3f} A'.format(bitwisecurrent(data)))
        return(self._bitwisecurrent(data))

    def meaPowerOutL(self):
        pi=pigpio.pi()
        ina=pi.i2c_open(1,0x45)
        try:
            pi.i2c_write_device(ina,(0b00000000,0b01000001,0b00100111)) #write configuration register, 
        #1 average, 
        #1.1 ms bus voltage conversion time, 
        #1.1 ms shunt voltage conversion time,
        #shunt and bus continuous mode
            pi.i2c_write_device(ina,(0b00000101,0b00000101,0b00000000)) #write calibration register
        #200 muA/bit = 1280 with 0.02 Ohms
            time.sleep(0.01)
            pi.i2c_write_byte(ina,0b0011) #set address register to power register
            count,data=pi.i2c_read_device(ina,2) #read power
        except pigpio.error as e: 
            pi.i2c_close(ina)
            pi.stop()
            return 0
        except:
            pi.i2c_close(ina)
            pi.stop()
            raise
        pi.i2c_close(ina)
        pi.stop()
        #print('measured power, it is {:.3f} W'.format(bitwisepower(data)))
        return(self._bitwisepower(data))
