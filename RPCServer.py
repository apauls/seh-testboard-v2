"""

@author: Martin
"""

#from SimpleXMLRPCServer import SimpleXMLRPCServer #python 2
#from SimpleXMLRPCServer import SimpleXMLRPCRequestHandler
from xmlrpc.server import SimpleXMLRPCServer #python 3
from xmlrpc.server import SimpleXMLRPCRequestHandler

from adc import adc
from ina import ina
from compact import LVReader
from fpga_sequence import fpga_sequence
from gpio import gpio
from fpga import fpga
from hvV2 import hvV2

# Restrict to a particular path.
class RequestHandler(SimpleXMLRPCRequestHandler):
    rpc_paths = ('/RPC2',) #RPC2 standard for XML-RPC

# Create server
server = SimpleXMLRPCServer(("0.0.0.0", 8000), requestHandler=RequestHandler,allow_none=True) # cannot reach port from external PC if using "localhost"!
server.register_introspection_functions() #system.listMethods(), system.methodHelp(), system.methodSignature()



# Register a function under a different name (demo)
def adder_function(x,y):
    return x + y
server.register_function(adder_function, 'add')

# Register an instance; all the methods of the instance are
# published as XML-RPC methods (in this case, just 'div').
class MyFuncs:
    def div(self, x, y):
        return x // y
    def pow(self,x,y):
        return x**y
#
#server.register_instance(MyFuncs())

## start registering Alexanders SEH-Testboard functions
# attention: only one instance can be registered! 
# This will not work: 
# server.register_instance(adc())
# server.register_instance(ina())
# ...
# We need the following workaround:

class AllFuncs(adc, ina, LVReader, fpga_sequence,gpio,fpga,hvV2):
    pass

server.register_instance(AllFuncs()) #() creates Instance of the class
#server.register_function(LVReader.LV_Values1)

# Run the server's main loop
server.serve_forever()
