# How to set up a new Raspberry Pi for a Testboard:

## a) There is a lazy option: 
Just use a tool like Win32 Disk Manager to create a 1:1 copy of a working SD card. 
You can then use the existing hardware, access the Pi remotely and configure it for a new network card (see 4-7 below).

## b) Set up a new SD card:

1. Follow the instructions [here](https://www.raspberrypi.org/documentation/installation/installing-images/README.md) to install the operating system on the SD card 
2. You can now insert the SD card into a Pi and boot it. You will need all the accessories for Desktop operation in order to perform the required actions. 
You might be asked to add a password, the user is normally just "pi".
3. Enable the interfaces "ssh", "i2c", and "spi" using the settings in the GUI, or use `sudo raspi-config` in a terminal.
4. To connect to a wired network you need to register your Pi's network card MAC address with the IT support. They will tell you the corresponding IP address. 
5. Do `ifconfig` in a terminal to check the network name (should be e.g. "eth0").
6. Edit "/etc/dhcpcd.conf" (as sudo): At the end of the file put 

        
        interface eth0 (edit your network name)
        static ip_address=134.61.xxx.xxx/24 (edit IP address)
        static routers=134.130.5.1
        static domain_name_servers=134.130.5.1
        
        
7. After a reboot you should now be able to connect to the internet via the new network card. 
8. Check that you connect to the Pi from your DAQ-PC via SSH. For key-based access see tutorials like [this](https://www.raspberrypi.org/documentation/remote-access/ssh/passwordless.md).
9. Now do `cd ~` and copy this git respository `git clone https://gitlab.cern.ch/apauls/seh-testboard-v2.git`. 
10. Do `crontab -e` and add the lines 

        @reboot sudo pigpiod
        @reboot python3 seh-testboard-v2/RPCServer.py
        @reboot python seh-testboard-v2/startPi.py

to set up the Daemon for the testboard interfaces and to start the RPC server whenever the Pi goes up.
11. It might be useful to have ipython installed. Do `sudo apt-get install ipython ipython3`.
    
30.09.2020
    
