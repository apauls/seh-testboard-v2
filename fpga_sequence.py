# -*- coding: utf-8 -*-
"""
Created on Tue Feb  5 12:08:19 2019

@author: Alexander
"""

import opcodes
import time
class fpga_sequence:
    def clocks(self):
        opcodes.opcode_01()
        time.sleep(0.000150)
        rightclock=opcodes.opcode_02() #Beschreibung fehlt
        leftclock=opcodes.opcode_06() #Beschreibung fehlt
        print('{:<25}{:>016b}{:>10d}'.format('Right Clock Counter: ',rightclock,rightclock))
        print('{:<25}{:>016b}{:>10d}'.format('Left Clock Counter: ',leftclock,leftclock))
        return_val = (rightclock, leftclock)
        return return_val

    def cbc_resets(self, waiting_time=10):
        opcodes.opcode_08()
        opcodes.opcode_09()
        time.sleep(waiting_time)
        rightcounter=opcodes.opcode_10() #Beschreibung fehlt
        leftcounter=opcodes.opcode_14() #Beschreibung fehlt
        print('{:<25}{:>016b}{:>10d}'.format('Right CBC Reset Counter: ',rightcounter,rightcounter))
        print('{:<25}{:>016b}{:>10d}'.format('Left CBC Reset Counter: ',leftcounter,leftcounter))
        return_val = (rightcounter, leftcounter)
        return return_val


    def cic_resets(self, waiting_time=10):
        opcodes.opcode_16()
        opcodes.opcode_17()
        time.sleep(waiting_time)
        rightcounter=opcodes.opcode_18()
        leftcounter=opcodes.opcode_22()
        print('{:<25}{:>016b}{:>10d}'.format('Right CIC Reset Counter: ',rightcounter,rightcounter))
        print('{:<25}{:>016b}{:>10d}'.format('Left CIC Reset Counter: ',leftcounter,leftcounter))


def Resetcounter(waiting_time=10):
    opcodes.opcode_08()
    opcodes.opcode_09()
    opcodes.opcode_16()
    opcodes.opcode_17()
    time.sleep(waiting_time)
    rightcountercbc=opcodes.opcode_10()
    leftcountercbc=opcodes.opcode_14()
    rightcountercic=opcodes.opcode_18()
    leftcountercic=opcodes.opcode_22()
    print('{:<25}{:>016b}{:>10d}'.format('Right CBC Reset Counter: ',rightcountercbc,rightcountercbc))
    print('{:<25}{:>016b}{:>10d}'.format('Left CBC Reset Counter: ',leftcountercbc,leftcountercbc))    
    print('{:<25}{:>016b}{:>10d}'.format('Right CIC Reset Counter: ',rightcountercic,rightcountercic))
    print('{:<25}{:>016b}{:>10d}'.format('Left CIC Reset Counter: ',leftcountercic,leftcountercic))    
    
    
    
    
    
