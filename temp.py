# -*- coding: utf-8 -*-
"""
Created on Fri Mar 16 14:17:57 2018

@author: Alexander
"""
import pigpio

def bitwisetemp(list):
    if list[0]<128:
        return ((list[0]<<6) | (list[1]>>2))   
    
    else:
        return ((-1<<14) | (list[0]<<6) | (list[1]>>2))
    
def mea():
    pi=pigpio.pi() # connect to local Pi

    spi1=pi.spi_open(1,50000) # open SPI device 1 with 50000 bits per second (32K to 30 M possible)

    temp=pi.spi_read(spi1,2) # read 2 byte of data from the device
    print(temp)
    print(temp[1][0])
    pi.spi_close(spi1)

    pi.stop()
    print('measured temperature, it is '+str(bitwisetemp(temp[1])*0.03125 )+' C')
    return(bitwisetemp(temp[1]))
