# -*- coding: utf-8 -*-
"""
Created on Fri Mar 16 11:33:31 2018

@author: Alexander


all opcodes follow the same scheme:

def opcode_02(): #callable from fpga.py
    pi=pigpio.pi() #conect to local pi
    spi0=pi.spi_open(0,100000) #open spi device 
    pi.spi_write(spi0,b'\x02') #write the command byte to fpga
    (n,data)=pi.spi_read(spi0,2) #if needed read the 2 byte answer
    pi.spi_close(spi0) #close device
    pi.stop() #disconect from pi
    print('executed opcode 02, returned value is {:08b} {:08b}'.format(data[0],data[1])) #print status for debugging
    return(data) #return data or None
"""
import pigpio

def opcode_00():
    pi=pigpio.pi() 
    spi0=pi.spi_open(0,100000) 
    pi.spi_write(spi0,b'\x00')
    pi.spi_close(spi0)
    pi.stop()
    #print('executed opcode 00')
    return(None)
    
def opcode_01():
    pi=pigpio.pi() 
    spi0=pi.spi_open(0,100000) 
    pi.spi_write(spi0,b'\x01')
    pi.spi_close(spi0)
    pi.stop()
    #print('executed opcode 01')
    return(None)
    
def opcode_02():
    pi=pigpio.pi() 
    spi0=pi.spi_open(0,100000) 
    pi.spi_write(spi0,b'\x02')
    (n,data)=pi.spi_read(spi0,2)
    pi.spi_close(spi0)
    pi.stop()
    #print('executed opcode 02, returned value is {:08b} {:08b}'.format(data[0],data[1]))
    return((data[0]<<8) | data[1])

def opcode_06():
    pi=pigpio.pi() 
    spi0=pi.spi_open(0,100000) 
    pi.spi_write(spi0,b'\x06')
    (n,data)=pi.spi_read(spi0,2)
    pi.spi_close(spi0)
    pi.stop()
    #print('executed opcode 06, returned value is {:08b} {:08b}'.format(data[0],data[1]))
    return((data[0]<<8) | data[1])

def opcode_08():
    pi=pigpio.pi() 
    spi0=pi.spi_open(0,100000) 
    pi.spi_write(spi0,b'\x08')
    pi.spi_close(spi0)
    pi.stop()
    #print('executed opcode 08')
    return(None)

def opcode_09():
    pi=pigpio.pi() 
    spi0=pi.spi_open(0,100000) 
    pi.spi_write(spi0,b'\x09')
    pi.spi_close(spi0)
    pi.stop()
    #print('executed opcode 09')
    return(None)

def opcode_10():
    pi=pigpio.pi() 
    spi0=pi.spi_open(0,100000) 
    pi.spi_write(spi0,b'\x0a')
    (n,data)=pi.spi_read(spi0,2)
    pi.spi_close(spi0)
    pi.stop()
    #print('executed opcode 10, returned value is {:08b} {:08b}'.format(data[0],data[1]))
    return((data[0]<<8) | data[1])

def opcode_14():
    pi=pigpio.pi() 
    spi0=pi.spi_open(0,100000) 
    pi.spi_write(spi0,b'\x0e')
    (n,data)=pi.spi_read(spi0,2)
    pi.spi_close(spi0)
    pi.stop()
    #print('executed opcode 14, returned value is {:08b} {:08b}'.format(data[0],data[1]))
    return((data[0]<<8) | data[1])

def opcode_16():
    pi=pigpio.pi() 
    spi0=pi.spi_open(0,100000) 
    pi.spi_write(spi0,b'\x10')
    pi.spi_close(spi0)
    pi.stop()
    #print('executed opcode 16')
    return(None)

def opcode_17():
    pi=pigpio.pi() 
    spi0=pi.spi_open(0,100000) 
    pi.spi_write(spi0,b'\x11')
    pi.spi_close(spi0)
    pi.stop()
    #print('executed opcode 17')
    return(None)

def opcode_18():
    pi=pigpio.pi() 
    spi0=pi.spi_open(0,100000) 
    pi.spi_write(spi0,b'\x12')
    (n,data)=pi.spi_read(spi0,2)
    pi.spi_close(spi0)
    pi.stop()
    #print('executed opcode 18, returned value is {:08b} {:08b}'.format(data[0],data[1]))
    return((data[0]<<8) | data[1])

def opcode_22():
    pi=pigpio.pi() 
    spi0=pi.spi_open(0,100000) 
    pi.spi_write(spi0,b'\x16')
    (n,data)=pi.spi_read(spi0,2)
    pi.spi_close(spi0)
    pi.stop()
    #print('executed opcode 22, returned value is {:08b} {:08b}'.format(data[0],data[1]))
    return((data[0]<<8) | data[1])

def opcode_24():
    pi=pigpio.pi() 
    spi0=pi.spi_open(0,100000) 
    pi.spi_write(spi0,b'\x18')
    pi.spi_close(spi0)
    pi.stop()
    #print('executed opcode 24')
    return(None)

def opcode_25():
    pi=pigpio.pi() 
    spi0=pi.spi_open(0,100000) 
    pi.spi_write(spi0,b'\x19')
    pi.spi_close(spi0)
    pi.stop()
    #print('executed opcode 25')
    return(None)

def opcode_26():
    pi=pigpio.pi() 
    spi0=pi.spi_open(0,100000) 
    pi.spi_write(spi0,b'\x1a')
    (n,data)=pi.spi_read(spi0,2)
    pi.spi_close(spi0)
    pi.stop()
    #print('executed opcode 26, returned value is {:08b} {:08b}'.format(data[0],data[1]))
    return((data[0]<<8) | data[1]) #status

def opcode_28():
    pi=pigpio.pi() 
    spi0=pi.spi_open(0,100000) 
    pi.spi_write(spi0,b'\x1c')
    pi.spi_close(spi0)
    pi.stop()
    #print('executed opcode 28')
    return(None)

def opcode_29():
    pi=pigpio.pi() 
    spi0=pi.spi_open(0,100000) 
    pi.spi_write(spi0,b'\x1d')
    pi.spi_close(spi0)
    pi.stop()
    #print('executed opcode 29')
    return(None)

def opcode_32():
    pi=pigpio.pi() 
    spi0=pi.spi_open(0,100000) 
    pi.spi_write(spi0,b'\x20')
    pi.spi_close(spi0)
    pi.stop()
    #print('executed opcode 32')
    return(None)

def opcode_33():
    pi=pigpio.pi() 
    spi0=pi.spi_open(0,100000) 
    pi.spi_write(spi0,b'\x21')
    pi.spi_close(spi0)
    pi.stop()
    #print('executed opcode 33')
    return(None)

"""
def opcode_30():
    pi=pigpio.pi() 
    spi0=pi.spi_open(0,100000) 
    pi.spi_write(spi0,b'\x1e')
    (n,data)=pi.spi_read(spi0,2)
    pi.spi_close(spi0)
    pi.stop()
    #print('executed opcode 30, returned value is {:08b} {:08b}'.format(data[0],data[1]))
    return(data)

def opcode_34():
    pi=pigpio.pi() 
    spi0=pi.spi_open(0,100000) 
    pi.spi_write(spi0,b'\x22')
    (n,data)=pi.spi_read(spi0,2)
    pi.spi_close(spi0)
    pi.stop()
    #print('executed opcode 34, returned value is {:08b} {:08b}'.format(data[0],data[1]))
    return(data)
"""